<!doctype html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Welcome to Heritage</title>
    <link rel="stylesheet" href="css/main.css" />
    <link rel="stylesheet" href="css/foundation.css" />
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,100,300,700' rel='stylesheet' type='text/css'>
    <script src="js/vendor/modernizr.js"></script>
  </head>
  <body>
  <div class="wrapper">
    <div class="row">
      <?php
        include("header-rooms.php");
      ?>        
    </div>
    
    <div class="row room-contact">
      <div class="large-2 columns"></div>
      <div class="large-8 small-12 columns">
        <div class="row rooms-content">
          <div class="large-1 columns"></div>                
          <div class="large-10 small-12 columns">  
            <div class="row">
              <h1 class="rooms" style="text-align: center; color: #000; margin-bottom: 10%;">Contact Us</h1>
            </div>
            <div class="row main-content">
              <img src="img/white-space.jpg" width="100%" />  
              <div class="row" style="margin-top: 5%;">
              <p>
                For general enquiries including bookings,
                <br />
                please contact us at: <a href="">xxx@thhg.sg</a>
              </p>
              <p>
                For media queries,
                <br />
                please contact us at <a href="mailto:media@thhg.sg">media@thhg.sg</a>
              </p>
              </div>
            </div>
            <center><h5>#SoLoveTheFeeling</h5></center>
          </div>          
          <div class="large-1 columns"></div>
        </div>          
      </div>
      <div class="large-2 columns"></div>
    </div>

    <?php
      include("footer-rooms.php");
    ?>    
  </div>

    <script src="js/vendor/jquery.js"></script>
    <script src="js/foundation.min.js"></script>
    <script>
      $(document).foundation();
    </script>
  </body>
</html>