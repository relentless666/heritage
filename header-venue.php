
<div class="large-12 columns mobile-12">
<nav class="top-bar mobile" data-topbar role="navigation">
  <ul class="title-area"> 
    <li class="name"></li>
        <li class="toggle-topbar menu-icon"><a href="#"><span>Menu</span></a></li>
  </ul>
  <section class="top-bar-section">
    <ul class="center-buttons">
      <li><a href="about.php">About Us</a></li>
      <li><a href="events.php">Events, Party &amp; Functions</a></li>
      <li><a href="weddings.php">Weddings</a></li>
      <li><a href="gallery.php">Gallery</a></li>
      <li><a href="rates.php">Rates</a></li>
      <li><a href="bulletin.php">News</a></li>
      <li><a href="clients.php">Our Clients</a></li>
      <li><a href="contactus.php">Contact Us</a></li>
      <li><a href="faqs.php">FAQ</a></li>
    </ul>
  </section>  
</nav>

<header>
<div class="row">
  <div class="large-12 columns">
    <div class="large-1 columns"></div>
    <div class="large-10 columns">
      <div class="large-1 columns"></div>
      <div class="large-10 columns">
        <div class="large-1 columns"></div>
        <div class="large-10 columns">
            <center><a href="venue.php"><img src="img/venue-logo.png" /></a></center>
            <p class="header-href">For Our Rooms<br />Click <a href="rooms.php">Here</a></p>
          </div>
          <div class="large-1 columns"></div>
        </div>  
        <div class="large-1 columns"></div> 
      </div>
      <div class="large-1 columns"></div>
   </div>
</div>
</header>

<nav class="top-bar desktop" data-topbar role="navigation">
  <ul class="title-area"> 
    <li></li> 
  </ul>
  <section class="top-bar-section">
    <ul class="center-buttons">
      <li><a href="about.php">About Us</a></li>
      <li><a href="events.php">Events, Party &amp; Functions</a></li>
      <li><a href="weddings.php">Weddings</a></li>
      <li><a href="gallery.php">Gallery</a></li>
      <li><a href="rates.php">Rates</a></li>
      <li><a href="bulletin.php">News</a></li>
      <li><a href="clients.php">Our Clients</a></li>
      <li><a href="contactus.php">Contact Us</a></li>
      <li><a href="faqs.php">FAQ</a></li>
    </ul>
  </section>  
</nav>
</div>