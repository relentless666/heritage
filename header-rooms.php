
<div class="large-12 columns mobile-12">
<nav class="top-bar mobile" data-topbar role="navigation">
	<ul class="title-area">	
		<li class="name"></li>
      	<li class="toggle-topbar menu-icon"><a href="#"><span>Menu</span></a></li>
	</ul>
	<section class="top-bar-section">
		<ul class="center-buttons">
			<li><a href="rooms.php">About Us</a></li>
			<li><a href="roomsnrates.php">Rooms &amp; Rates</a></li>
			<li><a href="news.php">News</a></li>
			<li><a href="contact.php">Contact Us</a></li>
			<li><a href="faq.php">FAQ</a></li>
		</ul>
	</section>	
</nav>

<header>
<div class="row">
	<div class="large-12 columns">
		<div class="large-1 columns"></div>
		<div class="large-10 columns">
			<div class="large-1 columns"></div>
			<div class="large-10 columns">
				<div class="large-1 columns"></div>
				<div class="large-10 columns">
		  			<center><a href="index.html"><img src="img/logo.png" /></a></center>
		  			<p class="header-href">For Our Venue<br />Click <a href="venue.php">Here</a></p>
		  		</div>
		  		<div class="large-1 columns"></div>
		  	</div>	
		  	<div class="large-1 columns"></div>	
	  	</div>
	  	<div class="large-1 columns"></div>
	 </div>
</div>
</header>

<nav class="top-bar desktop" data-topbar role="navigation">
	<ul class="title-area">	
		<li></li>	
	</ul>
	<section class="top-bar-section">
		<ul class="center-buttons">
			<li><a href="rooms.php">About Us</a></li>
			<li><a href="roomsnrates.php">Rooms &amp; Rates</a></li>
			<li><a href="news.php">News</a></li>
			<li><a href="contact.php">Contact Us</a></li>
			<li><a href="faq.php">FAQ</a></li>
		</ul>
	</section>	
</nav>
</div>