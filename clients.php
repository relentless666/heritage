<!doctype html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Welcome to Heritage</title>
    <link rel="stylesheet" href="css/main.css" />
    <link rel="stylesheet" href="css/foundation.css" />
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,100,300,500,700' rel='stylesheet' type='text/css'>
    <script src="js/vendor/modernizr.js"></script>
  </head>
  <body>
  <div class="wrapper">
    <div class="row">
      <?php
        include("header-venue.php");
      ?>        
    </div>
    
    <div class="row">
      <div class="large-12 columns clients-venue">
        <div class="large-1 columns"></div>
        <div class="large-10 columns">
          <div class="">
            <div class="large-1 columns"></div>
            <div class="large-10 columns body-content">
              <div class="row">
                <div class="large-1 columns"></div>                
                <div class="large-4 columns index-content">
                  <h1 class="venue blacktext">Our Clients</h1>
                  
                  
                  <p>No Content</p> 

 
                </div>
                <div class="large-6 columns"></div>
                <div class="large-1 columns"></div>
              </div>
              <div class="gap"></div>
            </div>
            <div class="large-1 columns"></div>
          </div>  
        </div>
        <div class="large-1 columns"></div>
      </div>
    </div>

    <?php
      include("footer-venue.php");
    ?>    
  </div>

    <script src="js/vendor/jquery.js"></script>
    <script src="js/foundation.min.js"></script>
    <script>
      $(document).foundation();
    </script>
  </body>
</html>