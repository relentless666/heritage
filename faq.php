<!doctype html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Welcome to Heritage</title>
    <link rel="stylesheet" href="css/main.css" />
    <link rel="stylesheet" href="css/foundation.css" />
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,100,300,700' rel='stylesheet' type='text/css'>
    <script src="js/vendor/modernizr.js"></script>
  </head>
  <body>
  <div class="wrapper">
    <div class="row">
      <?php
        include("header-rooms.php");
      ?>        
    </div>
    
    <div class="row room-faq">
      <div class="large-2 columns"></div>
      <div class="large-8 small-12 columns">
        <div class="row rooms-content">
          <div class="large-1 columns"></div>                
          <div class="large-10 small-12 columns">  
            <div class="row">
              <h1 class="rooms" style="text-align: center; color: #fff; margin-bottom: 10%;">FAQ</h1>
            </div>
            <div class="row main-content">
              <div class="large-6 columns">
                <h4>Q. Where is The Rucksack Heritage?</h4>
                <p>A. The Rucksack Heritage is a boutique hotel nestled in the cosy enclave of Joo Chiat-Katong, in Singapore. </p>
                <h4>Q. How many people can The Rucksack Heritage accommodate?</h4>
                <p>A. There are five rooms in total which accommodate up to 10 adults in total, based on occupancy of 2 persons per room.</p>
                <h4>Q. Can guests book the rooms even if we don’t hold an event there? </h4>
                <p>A. Yes, the accommodation may be booked even without hiring the venue out for any events. It makes for a great staycation!</p>
              </div>
              <div class="large-6 columns">
                <h4>Q. Can an event be held at the venue if guests are in residence? </h4>
                <p>A. Yes, we do host events even if there are guests in residence so long as the event does not impose on the guests’ experience. In fact, many of our guests are attendees of the events held at the event, so the venue works to their advantage. </p>
                <h4>Q. Is breakfast included in stays at The Rucksack Heritage? </h4>
                <p>A. Yes, breakfast is included with all stays at The Rucksack Heritage. </p>
                <h4>Q. Is there complimentary WiFi available at The Rucksack Heritage? </h4>
                <p>A. Yes, complimentary WiFi is available throughout the property. </p>
              </div>
            </div>
            <center><h5>#SoLoveTheFeeling</h5></center>
          </div>          
          <div class="large-1 columns"></div>
        </div>          
      </div>
      <div class="large-2 columns"></div>
    </div>

    <?php
      include("footer-rooms.php");
    ?>    
  </div>

    <script src="js/vendor/jquery.js"></script>
    <script src="js/foundation.min.js"></script>
    <script>
      $(document).foundation();
    </script>
  </body>
</html>