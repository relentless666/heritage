<!doctype html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Welcome to Heritage</title>
    <link rel="stylesheet" href="css/main.css" />
    <link rel="stylesheet" href="css/foundation.css" />
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,100,300,700' rel='stylesheet' type='text/css'>
    <script src="js/vendor/modernizr.js"></script>
  </head>
  <body>
  <div class="wrapper">
    <div class="row">
      <?php
        include("header-rooms.php");
      ?>        
    </div>
    
    <div class="row room-rnr">
      <div class="large-2 columns"></div>
      <div class="large-8 small-12 columns">
        <div class="row rooms-content">
          <div class="large-1 columns"></div>                
          <div class="large-10 small-12 columns">  
            <div class="row">
              <h1 class="rooms" style="float: right; color: #000;">Rooms &amp; Rates</h1>
            </div>
            <div class="row main-content">
              <div class="row">
                <div class="large-6 columns">
                  <p>
                    Luxurious, four-poster beds filled with comforting bedding and pillows is the first sight to greet guests in all of our well-appointed suites at The Rucksack Heritage.
                  </p>
                  <p>
                    Dressed in classic Peranakan style, all rooms come with complimentary coffee and tea, walk-in wardrobes, sitting areas, LCD televisions and other creature comforts of the modern world at arms’ length, The Rucksack Heritage offers three suite types to choose from:
                  </p> 
                  <hr>
                </div>
                <div class="large-6 columns">
                  <h2>
                    Family Suite
                  </h2>
                  <img src="img/white-space.jpg" width="100%" /> 
                  <p align="right">
                    Two rooms, with adjoining bathroom
                    <br />
                    <span style="font-size: 19px; font-weight: 700;">S$450 per night</span>
                  </p>
                </div>  
              </div>
              <div class="row">
                <div class="large-6 columns">
                  <h2>
                    Junior Suite
                  </h2>
                  <img src="img/white-space.jpg" width="100%" />  
                  <p align="right">
                    Three rooms available
                    <br />
                    Queen size beds
                    <br />
                    <span style="font-size: 19px; font-weight: 700;">S$300 per night</span>
                  </p>    
                </div>
                <div class="large-6 columns">                  
                  <h2>
                    Presidential Suite
                  </h2>
                  <img src="img/white-space.jpg" width="100%" />
                  <p align="right">
                    One suite
                    <br />
                    King Size bed and daybed
                    <br />
                    Double bathroom with bathtub
                    <br />
                    <span style="font-size: 19px; font-weight: 700;">S$600 per night</span>
                  </p>
                </div>                  
              </div>  
            </div>
            <center><h5>#SoLoveTheFeeling</h5></center>
          </div>          
          <div class="large-1 columns"></div>
        </div>          
      </div>
      <div class="large-2 columns"></div>
    </div>

    <?php
      include("footer-rooms.php");
    ?>    
  </div>

    <script src="js/vendor/jquery.js"></script>
    <script src="js/foundation.min.js"></script>
    <script>
      $(document).foundation();
    </script>
  </body>
</html>