<!doctype html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Welcome to Heritage</title>
    <link rel="stylesheet" href="css/main.css" />
    <link rel="stylesheet" href="css/foundation.css" />
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,100,300,500,700' rel='stylesheet' type='text/css'>
    <script src="js/vendor/modernizr.js"></script>
  </head>
  <body>
  <div class="wrapper">
    <div class="row">
      <?php
        include("header-venue.php");
      ?>        
    </div>
    
    <div class="row">
      <div class="large-12 columns contact-venue">
        <div class="large-1 columns"></div>
        <div class="large-10 columns">
          <div class="">
            <div class="large-1 columns"></div>
            <div class="large-10 columns body-content">
              <div class="row">
                <div class="large-1 columns"></div>                
                <div class="large-4 columns index-content">
                  <h1 class="venue blacktext">Contact Us</h1>
                  <form data-abide="ajax" id="myForm">
                    <div class="contactform">
                    <div class="name-field">
                      <label>Your name <small>required</small>
                      <input type="text" required id="name">
                      </label>
                      <small class="error">Name is required.</small>
                    </div>
                    <div class="email-field">
                      <label>Email <small>required</small>
                      <input type="email" required id="email">
                      </label>
                      <small class="error">E-mail address is required.</small>
                    </div>
                    <div class="text-field">
                      <label>Your message <small>required</small>
                      <textarea required id="message"></textarea>
                      </label>
                      <small class="error">Your message is required.</small>
                    </div>
                      <button type="submit">Submit</button>
                    </div>  
                  </form>
                  
                  <p>For general enquiries including bookings, please contact us at: <a href="xxx@thhg.sg">xxx@thhg.sg</a> / (65) xxxx xxxx</p> 

                  <p>Please note that viewing is by appointment only, between 10 am to 5pm. To make an appointment, please contact us at: <a href="xxx@thhg.sg">xxx@thhg.sg</a> / (65) xxxx xxxx</p>  
                </div>
                <div class="large-6 columns"></div>
                <div class="large-1 columns"></div>
              </div>
              <div class="gap"></div>
            </div>
            <div class="large-1 columns"></div>
          </div>  
        </div>
        <div class="large-1 columns"></div>
      </div>
    </div>

    <?php
      include("footer-venue.php");
    ?>    
    </div>

    <script src="js/vendor/jquery.js"></script>
    <script src="js/foundation.min.js"></script>
    <script src="js/foundation/foundation.abide.js"></script>
    <script>
      $(document).foundation();
    </script>
    <script>
$('#myForm')  
  .on('valid.fndtn.abide', function () {

    var name = $("input#name").val();  
    var email = $("input#email").val();
    var message = $("textarea#message").val();


        //Data for response 
    var dataString = 'name=' + name + 
             '&email=' + email + 
             '&message=' + message;
             
    
    //Begin Ajax call
    $.ajax({
      type: "POST",
      url: "mail.php",
      data: dataString,
      success: function() {
        $('.contactform').html("<div id='thanks'></div>");
            $('#thanks').html("<h1>Thanks!</h1>")
            .append("<p>Dear "+ name +"!, We will contact to you soon.</p>")
            .hide()
            .fadeIn(1500);
      },

    });//ajax call
    return false;  

  });
    </script>
  </body>
</html>