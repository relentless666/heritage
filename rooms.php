<!doctype html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Welcome to Heritage</title>
    <link rel="stylesheet" href="css/main.css" />
    <link rel="stylesheet" href="css/foundation.css" />
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,100,300,700' rel='stylesheet' type='text/css'>
    <script src="js/vendor/modernizr.js"></script>
  </head>
  <body>
  <div class="wrapper">
    <div class="row">
      <?php
        include("header-rooms.php");
      ?>        
    </div>
    
    <div class="row room-about">
      <div class="large-2 columns"></div>
      <div class="large-8 small-12 columns">
        <div class="row body-content">
          <div class="large-1 columns"></div>                
          <div class="large-5 large-push-5 columns">
            <h1 class="rooms" style="float: right;">Our Rooms</h1>
          </div>
          <div class="large-5 large-pull-5 columns main-content">
            <p>The Rucksack Heritage is a boutique hotel nestled in the culturally rich enclave of Joo Chiat, and part of The Hip &amp; Happening Group’s ‘The Rucksack’ line of hotels comprising a carefully curated collection of boutique properties.</p> 

            <p>The Rucksack Heritage offers five well-appointed suites in a contemporary Peranakan style. Luxurious, four-poster beds complete with superior bedding take centre stage in the larger suites, all of which feature a generous use of space, walk in wardrobes, state of the art audio-visual amenities as well as complimentary WiFi.</p>

            <p>The hotel has a lap pool with an al-fresco sitting area and a kitchenette set within a cosy garden fringed with hibiscus flowers, and which is annexed to 25 Chapel Road, a restored, pre-war Art-Deco style bungalow that serves as a private event venue.</p>

            <p>The hotel caters to anyone who seeks a unique staycation, whether to reminisce about ‘good ole days’, or for an escape from the city yet be a stone’s throw away from it, or travellers who would like to immerse themselves in a unique cultural enclave surrounded by great eats all round.</p>

            <p>Either way, welcome to The Rucksack Heritage.   We hope you enjoy your time with us.</p>
          </div>
          <div class="large-1 columns"></div>
        </div>          
      </div>
      <div class="large-2 columns"></div>
    </div>

    <?php
      include("footer-rooms.php");
    ?>    
  </div>

    <script src="js/vendor/jquery.js"></script>
    <script src="js/foundation.min.js"></script>
    <script>
      $(document).foundation();
    </script>
  </body>
</html>