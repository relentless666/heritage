<!doctype html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Welcome to Heritage</title>
    <link rel="stylesheet" href="css/main.css" />
    <link rel="stylesheet" href="css/foundation.css" />
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,100,300,500,700' rel='stylesheet' type='text/css'>
    <script src="js/vendor/modernizr.js"></script>
  </head>
  <body>
  <div class="wrapper">
    <div class="row">
      <?php
        include("header-venue.php");
      ?>        
    </div>
    
    <div class="row">
      <div class="large-12 columns faqs-venue">
        <div class="large-1 columns"></div>
        <div class="large-10 columns">
          <div class="">
            <div class="large-1 columns"></div>
            <div class="large-10 columns body-content">
              <div class="row">
                <div class="large-1 columns"></div>                
                <div class="large-4 columns index-content">
                  <h1 class="venue blacktext">FAQs</h1>
                  <p><b>Q. Is parking available at the venue?</b></p>

                  <p>A. Yes, we do have and can offer the limited parking lots at our venue at no cost, up until 10pm. Valet parking arrangements may also be made. Please speak with us about your requirements so we may arrange the necessary to suit your needs.</p>

                  <p><b>Q. What are your rates for a full-day event like?</b></p>

                  <p>A.We typically base our venue hire on the standard rate of S$240 per 3 hour-block. If you have specific needs, please speak with us so we may develop a package to suit your needs.</p>

                  <p><b>Q. Can I hold a fitness workout session at 25 @ Chapel Road?</b></p> 

                  <p>A. Yes, the venue is very versatile and can accommodate every kind of event, space permitting.</p>

                </div>
                <div class="large-6 columns"></div>
                <div class="large-1 columns"></div>
              </div>
              <div class="gap"></div>
            </div>
            <div class="large-1 columns"></div>
          </div>  
        </div>
        <div class="large-1 columns"></div>
      </div>
    </div>

    <?php
      include("footer-venue.php");
    ?>    
  </div>

    <script src="js/vendor/jquery.js"></script>
    <script src="js/foundation.min.js"></script>
    <script>
      $(document).foundation();
    </script>
  </body>
</html>