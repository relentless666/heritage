<!doctype html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Welcome to Heritage</title>
    <link rel="stylesheet" href="css/main.css" />
    <link rel="stylesheet" href="css/foundation.css" />
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,100,300,500,700' rel='stylesheet' type='text/css'>
    <script src="js/vendor/modernizr.js"></script>
  </head>
  <body>
  <div class="wrapper">
    <div class="row">
      <?php
        include("header-venue.php");
      ?>        
    </div>
    
    <div class="row">
      <div class="large-12 columns content-venue">
        <div class="large-1 columns"></div>
        <div class="large-10 columns">
          <div class="">
            <div class="large-1 columns"></div>
            <div class="large-10 columns body-content">
              <div class="row">
                <div class="large-1 columns"></div>                
                <div class="large-4 columns index-content">
                  <h1 class="venue blacktext">Our Venue</h1>
                  <p>We are an atypical event venue housed in a recently refreshed Art Deco style bungalow nestled in the culturally rich enclave of Joo Chiat.</p> 

                  <p>Conferred the URA Architectural Heritage Award in 2010, the property comprises the original bungalow and a modern wing complete with bedrooms, bathrooms and a lap-pool which have been added in a responsible fashion to complement the existing pre-war building.</p> 

                  <p>The main bungalow and its grounds have played host to a variety of occasions in the past, and today, is available for hire as a venue to host business meetings, private parties as well as weddings.</p>

                  <p>Within the bungalow, a meeting hall and rooms are a sophisticated yet homespun amalgam of contemporary style and nostalgia. Whether you wish to hold an intimate gathering, or are planning a grand cocktail reception, 25 @ Chapel Road offers a unique perspective and refreshing experience for you and your guests.</p> 

                  <p>If you’re keen to know more about the kind of events we are able to accommodate, please click on the relevant links below.</p>  
                </div>
                <div class="large-6 columns"></div>
                <div class="large-1 columns"></div>
              </div>
              <div class="gap"></div>
            </div>
            <div class="large-1 columns"></div>
          </div>  
        </div>
        <div class="large-1 columns"></div>
      </div>
    </div>

    <?php
      include("footer-venue.php");
    ?>    
  </div>

    <script src="js/vendor/jquery.js"></script>
    <script src="js/foundation.min.js"></script>
    <script>
      $(document).foundation();
    </script>
  </body>
</html>