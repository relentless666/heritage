<!doctype html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Welcome to Heritage</title>
    <link rel="stylesheet" href="css/main.css" />
    <link rel="stylesheet" href="css/foundation.css" />
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,100,300,500,700' rel='stylesheet' type='text/css'>
    <script src="js/vendor/modernizr.js"></script>
  </head>
  <body>
  <div class="wrapper">
    <div class="row">
      <?php
        include("header-venue.php");
      ?>        
    </div>
    
    <div class="row">
      <div class="large-12 columns events-venue">
        <div class="large-1 columns"></div>
        <div class="large-10 columns">
          <div class="">
            <div class="large-1 columns"></div>
            <div class="large-10 columns body-content">
              <div class="row">
                <div class="large-1 columns"></div>                
                <div class="large-4 columns index-content">
                  <h1 class="venue blacktext">Events<span><h4 class="blacktext">– Meetings, Parties, Fun Functions</h4></span></h1>
                  <p>We have played host to a variety of occasions in the past, and today, are available as a venue to host a plethora of events such as:</p> 
                  <ul>
                    <li>Networking events</li> 
                    <li>Seminars</li>
                    <li>Sales Presentations</li> 
                    <li>Workshops</li>
                    <li>Baby showers</li> 
                    <li>Birthday Parties</li> 
                    <li>Product Launches</li> 
                    <li>Corporate events, parties, training sessions,       retreats, general functions</li> 
                    <li>Conferences</li>
                    <li>Dinner & Dances</li> 
                    <li>Family/Other get-togethers</li> 
                    <li>Fitness functions (e.g. yoga classes, retreats)</li> 
                  </ul>
                </div>
                <div class="large-6 columns"></div>
                <div class="large-1 columns"></div>
              </div>
              <div class="gap"></div>
            </div>
            <div class="large-1 columns"></div>
          </div>  
        </div>
        <div class="large-1 columns"></div>
      </div>
    </div>

    <?php
      include("footer-venue.php");
    ?>    
  </div>

    <script src="js/vendor/jquery.js"></script>
    <script src="js/foundation.min.js"></script>
    <script>
      $(document).foundation();
    </script>
  </body>
</html>